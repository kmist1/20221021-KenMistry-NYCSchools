//
//  School.swift
//  20221021-KenMistry-NYCSchools
//
//  Created by Krunal Mistry on 10/21/22.
//

import Foundation


struct NYCSchool : Codable, Hashable {
    var dbn: String?
    var school_name: String?
    var city: String?
    var zip: String?
    var state_code: String?
    var school_email: String?
    var overview_paragraph: String?
    var location: String?
    var website: String?
    var phone_number: String?
}
