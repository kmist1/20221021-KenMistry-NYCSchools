//
//  SATScore.swift
//  20221021-KenMistry-NYCSchools
//
//  Created by Krunal Mistry on 10/21/22.
//

import Foundation

struct SATScore: Codable {
    var dbn: String?
    var school_name: String?
    var num_of_sat_test_takers: String?
    var sat_critical_reading_avg_score: String?
    var sat_math_avg_score: String?
    var sat_writing_avg_score: String?
}
