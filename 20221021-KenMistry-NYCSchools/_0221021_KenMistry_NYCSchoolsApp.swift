//
//  _0221021_KenMistry_NYCSchoolsApp.swift
//  20221021-KenMistry-NYCSchools
//
//  Created by Krunal Mistry on 10/21/22.
//

import SwiftUI

@main
struct _0221021_KenMistry_NYCSchoolsApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
