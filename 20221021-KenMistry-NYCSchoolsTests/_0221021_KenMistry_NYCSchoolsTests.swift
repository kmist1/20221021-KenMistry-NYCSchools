//
//  _0221021_KenMistry_NYCSchoolsTests.swift
//  20221021-KenMistry-NYCSchoolsTests
//
//  Created by Krunal Mistry on 10/21/22.
//

import XCTest
import SwiftUI

@testable import _0221021_KenMistry_NYCSchools

final class _0221021_KenMistry_NYCSchoolsTests: XCTestCase {
    @ObservedObject var viewModel: NYCSchoolViewModel = NYCSchoolViewModel()

    func testGetData() {
        let expectation = XCTestExpectation(description: "Must return true")
        let viewModel = viewModel

        viewModel.getData { success in
            if success {
                expectation.fulfill()
            } else {
                XCTFail("Couldn't get data")
            }
        }
    }

    func testGetSatData() {
        let expectation = XCTestExpectation(description: "Must return true")
        let viewModel = viewModel

        viewModel.getSatData(with: "21K728") { success in
            if success {
                expectation.fulfill()
            } else {
                XCTFail("Couldn't get data")
            }
        }
    }
}

