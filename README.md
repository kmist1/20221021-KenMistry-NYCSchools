20221021-KenMistry-NYCSchools

Features
Schools List View: 
fetches the school details and on success data is shown the Tableview. Data like, School name, City and Navigate is shown.

Detail View: Shows the school SAT Scores. Shows the school address, contact information and overview.

Codable Protocol: Have used the Codable protocol for JSON Serialization.

Unit Tests: Have written Unit tests.
